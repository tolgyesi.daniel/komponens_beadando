package hu.exam.beadando.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "course")
@Entity
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id", nullable = false)
    private Long courseId;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "course_instructor")
    private String courseInstructor;

    @Column(name = "course_time_date")
    private LocalDate courseTimeDate;

    @Column(name = "course_time_start")
    private LocalTime courseTimeStart;

    @Column(name = "course_time_end")
    private LocalTime courseTimeEnd;

    @Column(name = "course_once")
    private Boolean courseOnce;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room roomId;

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseInstructor() {
        return courseInstructor;
    }

    public void setCourseInstructor(String courseInstructor) {
        this.courseInstructor = courseInstructor;
    }

    public LocalDate getCourseTimeDate() {
        return courseTimeDate;
    }

    public void setCourseTimeDate(LocalDate courseTimeDate) {
        this.courseTimeDate = courseTimeDate;
    }

    public LocalTime getCourseTimeStart() {
        return courseTimeStart;
    }

    public void setCourseTimeStart(LocalTime courseTimeStart) {
        this.courseTimeStart = courseTimeStart;
    }

    public LocalTime getCourseTimeEnd() {
        return courseTimeEnd;
    }

    public void setCourseTimeEnd(LocalTime courseTimeEnd) {
        this.courseTimeEnd = courseTimeEnd;
    }

    public Boolean getCourseOnce() {
        return courseOnce;
    }

    public void setCourseOnce(Boolean courseOnce) {
        this.courseOnce = courseOnce;
    }

    public Room getRoomId() {
        return roomId;
    }

    public void setRoomId(Room roomId) {
        this.roomId = roomId;
    }
}
