package hu.exam.beadando.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "room")
@Entity
public class Room implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id", updatable = false, nullable = false)
    private Long roomId;

    @Column(name = "room_name")
    private String roomName;

    @Column(name = "room_level")
    private Integer roomLevel;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Integer getRoomLevel() {
        return roomLevel;
    }

    public void setRoomLevel(Integer roomLevel) {
        this.roomLevel = roomLevel;
    }
}
