package hu.exam.beadando.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import hu.exam.beadando.components.RoomForm;
import hu.exam.beadando.controllers.ApplicationContextHelper;
import hu.exam.beadando.controllers.Reloader;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.entity.Room;
import hu.exam.beadando.repository.CourseRepository;
import hu.exam.beadando.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Route
public class RoomsView extends AbstractView implements Reloader {

    @Autowired
    private RoomRepository roomRepository = ApplicationContextHelper.getRoomRepository();
    @Autowired
    private CourseRepository courseRepository = ApplicationContextHelper.getCourseRepository();

    private Grid<Room> roomGrid = new Grid<>();
    @Autowired
    private RoomForm roomForm = new RoomForm();

    private VerticalLayout localRoomLayout = new VerticalLayout();

    @Override
    public void processRefresh(){
        roomGrid.setItems(roomRepository.findAll());
    }

    @PostConstruct
    public void init(){
        initView();

        roomGrid = new Grid<>();
        roomGrid.setItems(roomRepository.findAll());
        roomGrid.addColumn(Room::getRoomName).setHeader("Tanterem neve");
        roomGrid.addColumn(Room::getRoomLevel).setHeader("Tanterem helye");

        roomGrid.asSingleSelect().addValueChangeListener(selectionEvent -> {
            if(selectionEvent.getValue() != null){
                localRoomLayout.removeAll();
                roomForm.setRoom(selectionEvent.getValue());
                localRoomInfo(selectionEvent.getValue());
            }
        });
        add(roomGrid, roomForm);
    }

    private void localRoomInfo(Room room){
        List<Course> usedRooms = courseRepository.findCourseByRoomId(room.getRoomId());
        List<String> localCourse = usedRooms.stream().map(Course::getCourseName).collect(Collectors.toList());

        Label title = new Label("Tanterem információk:");
        Label roomName = new Label("Tanterem megnevezése: "+room.getRoomName());
        Label roomLevel = new Label("Tanterem helye: "+room.getRoomLevel());
        Label roomCourses = new Label("");
        roomCourses.getElement().setProperty("innerHTML", "Tanteremben megtartando kurzusok:<br>"+getLocalCourse(localCourse));

        localRoomLayout.add(title, roomName, roomLevel, roomCourses);
        add(localRoomLayout);
        setLayouts(localRoomLayout, title);
    }

    private String getLocalCourse(List<String> localCourse){
        String allCourse = "<ul>";
        for(String getCourseName : localCourse){
            allCourse += "<li>" + getCourseName + "</li>";
        }
        allCourse += "</ul>";

        return allCourse;
    }

}
