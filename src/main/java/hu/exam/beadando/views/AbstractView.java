package hu.exam.beadando.views;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import hu.exam.beadando.components.AppMenuBar;


public abstract class AbstractView extends VerticalLayout {

    public void initView(){
        add(new AppMenuBar());
    }

    public void setLayouts(VerticalLayout localLayout, Label title){
        title.getStyle().set("padding", "15px 0 15px 0");
        title.setWidth("600px");
        title.getStyle().set("backgroundColor", "silver");
        title.getStyle().set("margin", "0");
        title.getStyle().set("padding", "0");
        localLayout.setWidth("100%");
        localLayout.getStyle().set("padding", "15px 0 0 35%");
        localLayout.getStyle().set("text-align", "center");
        localLayout.getStyle().set("font-size", "14pt");
    }

}
