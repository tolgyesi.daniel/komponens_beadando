package hu.exam.beadando.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.LocalDateRenderer;
import com.vaadin.flow.router.Route;
import hu.exam.beadando.components.CourseForm;
import hu.exam.beadando.controllers.ApplicationContextHelper;
import hu.exam.beadando.controllers.Reloader;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.repository.CourseRepository;
import hu.exam.beadando.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route
public class CoursesView extends AbstractView implements Reloader {

    @Autowired
    private CourseRepository courseRepository = ApplicationContextHelper.getCourseRepository();

    @Autowired
    private RoomRepository roomRepository = ApplicationContextHelper.getRoomRepository();

    private Grid<Course> courseGrid = new Grid<>();
    @Autowired
    private CourseForm courseForm = new CourseForm();

    private VerticalLayout localCourseLayout = new VerticalLayout();

    @Override
    public void processRefresh(){
        courseGrid.setItems(courseRepository.findAll());
    }

    @PostConstruct
    public void init(){
        initView();

        courseGrid = new Grid<>();
        courseGrid.setItems(courseRepository.findAll());
        courseGrid.addColumn(Course::getCourseName).setHeader("Kurzus neve");
        courseGrid.addColumn(Course::getCourseInstructor).setHeader("Kurzus vezetője");
        courseGrid.addColumn(new LocalDateRenderer<>(Course::getCourseTimeDate, "yyyy.MM.dd")).setHeader("Kurzus ideje");
        courseGrid.addColumn(Course::getCourseTimeStart).setHeader("Kurzus kezdete:");
        courseGrid.addColumn(Course::getCourseTimeEnd).setHeader("Kurzus vége:");

        courseGrid.asSingleSelect().addValueChangeListener(selectionEvent -> {
            if(selectionEvent.getValue() != null){
                localCourseLayout.removeAll();
                courseForm.setCourse(selectionEvent.getValue());
                localCourseInfo(selectionEvent.getValue());
            }
        });
        add(courseGrid, courseForm);
    }

    private void localCourseInfo(Course course){
        Label title = new Label("Kurzus információk:");
        Label courseName = new Label("Kurzus megnevezése: "+course.getCourseName());
        Label courseInstructor = new Label("Kurzus vezetője: "+course.getCourseInstructor());
        Label courseDate = new Label("Kurzus időpontja: "+course.getCourseTimeDate());
        Label courseStart = new Label("Kurzus kezdete: "+course.getCourseTimeStart());
        Label courseEnd = new Label("Kurzus vége: "+course.getCourseTimeEnd());
        Label courseOnce = new Label("Egyszer megtartandó kurzus: "+course.getCourseOnce());
        Label coursePlace = new Label("Kurzus helyszíne: "+roomRepository.findOneById(course.getRoomId()).getRoomName());

        localCourseLayout.add(title, courseName, courseInstructor, courseDate, courseStart, courseEnd, courseOnce, coursePlace);
        add(localCourseLayout);
        setLayouts(localCourseLayout, title);
    }

}
