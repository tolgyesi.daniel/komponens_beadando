package hu.exam.beadando.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import hu.exam.beadando.controllers.*;
import hu.exam.beadando.entity.Course;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class CourseForm extends HorizontalLayout {

    private Course course = new Course();
    private Reloader reloader;
    private CreateController createController = new CreateController();

    @PostConstruct
    private void init(){
        Button deleteButton = new Button("Törlés", new Icon(VaadinIcon.TRASH));
        deleteButton.addClickListener(buttonClickEvent -> {
            if(course.getCourseId() == null){
                Notification notification = new Notification("Nem választott ki kurzust!", 3000, Notification.Position.TOP_START);
                notification.open();
            }
            else {
                new DeleteController(course);
            }
        });

        Button addButton = new Button("Hozzáadás", new Icon(VaadinIcon.PLUS));
        addButton.addClickListener(buttonClickEvent -> {
            createController.courseCrudCreate();
        });

        Button editButton = new Button("Módosít", new Icon(VaadinIcon.EDIT));
        editButton.addClickListener(buttonClickEvent -> {
            if(course.getCourseId() == null){
                Notification notification = new Notification("Nem választott ki kurzust!", 3000, Notification.Position.TOP_START);
                notification.open();
            }
            else{
                new EditController(course);
            }
        });

        add(addButton, editButton, deleteButton);
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Reloader getReloader() {
        return reloader;
    }

    public void setReloader(Reloader reloader) {
        this.reloader = reloader;
    }
}
