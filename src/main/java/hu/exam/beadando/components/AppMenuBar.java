package hu.exam.beadando.components;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class AppMenuBar extends HorizontalLayout {
    public AppMenuBar(){
        localDesigner();

        buildAnchor("Főoldal", "/");
        buildAnchor("Kurzusok", "/courses");
        buildAnchor("Tantermek", "/rooms");
    }

    private void buildAnchor(String text, String href){
        Anchor anchor = new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        add(anchor);
    }

    private void localDesigner(){
        setWidth("100%");
        getStyle().set("padding", "20px");
        getStyle().set("margin", "0");
        getStyle().set("borderBottom", "1px solid black");
        getStyle().set("color", "white");
    }

}
