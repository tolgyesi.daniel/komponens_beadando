package hu.exam.beadando.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import hu.exam.beadando.controllers.CreateController;
import hu.exam.beadando.controllers.DeleteController;
import hu.exam.beadando.controllers.EditController;
import hu.exam.beadando.controllers.Reloader;
import hu.exam.beadando.entity.Room;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class RoomForm extends HorizontalLayout {

    private Room room = new Room();
    private Reloader reloader;
    private CreateController createController = new CreateController();

    @PostConstruct
    private void init(){

        Button deleteButton = new Button("Törlés", new Icon(VaadinIcon.TRASH));
        deleteButton.addClickListener(buttonClickEvent -> {
            if(room.getRoomId() == null){
                Notification notification = new Notification("Nem választott ki tantermet!", 3000, Notification.Position.TOP_START);
                notification.open();
            }
            else{
                new DeleteController(room);
            }
        });

        Button addButton = new Button("Hozzáadás", new Icon(VaadinIcon.PLUS));
        addButton.addClickListener(buttonClickEvent -> {
                createController.roomCrudCreate();
        });

        Button editButton = new Button("Módosít", new Icon(VaadinIcon.EDIT));
        editButton.addClickListener(buttonClickEvent -> {
            if(room.getRoomId() == null){
                Notification notification = new Notification("Nem választott ki tantermet!", 3000, Notification.Position.TOP_START);
                notification.open();
            }
            else{
                new EditController(room);
            }
        });

        add(addButton, editButton, deleteButton);
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Reloader getReloader() {
        return reloader;
    }

    public void setReloader(Reloader reloader) {
        this.reloader = reloader;
    }
}
