package hu.exam.beadando.controllers;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.entity.Room;

public class EditController extends AbstractCrudController {

    private Dialog editDialog = new Dialog();
    private Label dialogHeader = new Label("Módosítás");

    public EditController(Course course) {
        crudCourseEdit(course);
    }
    public EditController(Room room){crudRoomEdit(room);}

    private void crudCourseEdit(Course course){
        editDialog.open();

        confirmButton.addClickListener(buttonClickEvent -> {
            if(roomComboBox.isEmpty()){
                roomComboBox.setInvalid(true);
            }
            else if (courseBinder.writeBeanIfValid(course)) {
                courseRepository.courseUpdate(course);
                editDialog.close();
                UI.getCurrent().getPage().reload();
            }
        });

        cancelButton.addClickListener(buttonClickEvent -> {
            editDialog.close();
        });

        initRoomComboBox(course);
        initCourseBinder();

        courseNameText.setValue(course.getCourseName());
        courseInstructorText.setValue(course.getCourseInstructor());
        courseDatePicker.setValue(course.getCourseTimeDate());
        courseStartTimePicker.setValue(course.getCourseTimeStart());
        courseEndTimePicker.setValue(course.getCourseTimeEnd());
        courseOnce.setValue(course.getCourseOnce());
        roomComboBox.setValue(roomRepository.findOneById(course.getRoomId()));

        initComponents(editDialog, dialogHeader);
        editDialog.add(
                dialogHeader,
                courseNameText,
                courseInstructorText,
                courseDatePicker,
                courseStartTimePicker,
                courseEndTimePicker,
                courseOnce,
                roomComboBox,
                confirmButton,
                cancelButton
        );
    }

    private void crudRoomEdit(Room room){
        editDialog.open();

        confirmButton.addClickListener(buttonClickEvent -> {
            if(roomBinder.writeBeanIfValid(room)){
                roomRepository.roomUpdate(room);
                editDialog.close();
                UI.getCurrent().getPage().reload();
            }
        });

        cancelButton.addClickListener(buttonClickEvent -> {
            editDialog.close();
        });

        initRoomBinder();

        roomNameText.setValue(room.getRoomName());
        roomLevelText.setValue(room.getRoomLevel().toString());

        initComponents(editDialog, dialogHeader);
        editDialog.setHeight("300px");
        editDialog.add(
                dialogHeader,
                roomNameText,
                roomLevelText,
                confirmButton,
                cancelButton
        );
    }
}
