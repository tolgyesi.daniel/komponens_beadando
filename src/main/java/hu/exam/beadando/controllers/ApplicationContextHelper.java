package hu.exam.beadando.controllers;

import hu.exam.beadando.repository.CourseRepository;
import hu.exam.beadando.repository.RoomRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@ConditionalOnMissingBean(name = "applicationContextHelperOverrideBean")
@Component
public class ApplicationContextHelper implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(final ApplicationContext context) {
        ApplicationContextHelper.context = context;
    }

    public static <T> T getBean(final Class<T> requiredType) {
        return context.getBean(requiredType);
    }


    public static CourseRepository getCourseRepository(){
        return getBean(CourseRepository.class);
    }
    public static RoomRepository getRoomRepository() { return getBean(RoomRepository.class); }

}
