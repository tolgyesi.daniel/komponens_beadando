package hu.exam.beadando.controllers;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.entity.Room;

import java.util.List;
import java.util.stream.Collectors;

public class DeleteController extends AbstractCrudController {

    private Dialog deleteDialog = new Dialog();
    private Label dialogHeader = new Label("Törlés");
    private Label deleteLabel = new Label("");
    private Boolean usedRoom;

    public DeleteController(Course course) {
        crudCourseDelete(course);
    }
    public DeleteController(Room room){ crudRoomDelete(room); }

    private void crudCourseDelete(Course course){
        deleteDialog.open();
        usedRoom = false;

        confirmButton.addClickListener(buttonClickEvent -> {
            courseRepository.courseDelete(course);
            deleteDialog.close();
            UI.getCurrent().getPage().reload();
        });

        cancelButton.addClickListener(buttonClickEvent -> {
            deleteDialog.close();
        });

        initComponents(deleteDialog, dialogHeader);
        initCourseLocalComponents(course);
        initLocalComponent(usedRoom);
    }

    private void crudRoomDelete(Room room){
        deleteDialog.open();

        List<Course> usedRooms = courseRepository.findCourseByRoomId(room.getRoomId());

        if(usedRooms.isEmpty()){
            confirmButton.addClickListener(buttonClickEvent -> {
                roomRepository.roomDelete(room);
                deleteDialog.close();
                UI.getCurrent().getPage().reload();
            });
            cancelButton.addClickListener(buttonClickEvent -> {
                deleteDialog.close();
            });
            initRoomLocalComponents(room);
            usedRoom = false;
        }
        else{
            List<String> localCourse = usedRooms.stream().map(Course::getCourseName).collect(Collectors.toList());

            deleteLabel.getElement().setProperty(
                    "innerHTML", "<br><br>Nem törölheti a tantermet, mert a következő kurzusokhoz van rendelve:<br>"
                            +getLocalCourse(localCourse)+"<br>");

            cancelButton.setText("Bezárás");
            cancelButton.addClickListener(buttonClickEvent -> {
                deleteDialog.close();
            });
            usedRoom = true;
        }

        initComponents(deleteDialog, dialogHeader);
        initLocalComponent(usedRoom);
    }

    private void initCourseLocalComponents(Course course){
        deleteLabel.getElement().setProperty("innerHTML", "<br><br>Kurzus törlése: "+"<b>"+course.getCourseName()+"</b><br><br>Biztosan törli a kurzust?<br>");
    }

    private void initRoomLocalComponents(Room room){
        deleteLabel.getElement().setProperty("innerHTML", "<br><br>Tanterem törlése: "+"<b>"+room.getRoomName()+"</b><br><br>Biztosan törli a tantermet?<br>");
    }

    private void initLocalComponent(Boolean usedRoom){
        if(!usedRoom) {
            confirmButton.setText("Törlés");
            confirmButton.setIcon(new Icon(VaadinIcon.TRASH));
            confirmButton.getStyle().set("backgroundColor", "red");
            confirmButton.getStyle().set("color", "white");
            deleteDialog.setHeight("220px");

            deleteDialog.add(
                    dialogHeader,
                    deleteLabel,
                    confirmButton,
                    cancelButton
            );
        }
        else{
            deleteDialog.setHeight("350px");
            cancelButton.getStyle().set("marginLeft", "170px");
            deleteDialog.add(
                    dialogHeader,
                    deleteLabel,
                    cancelButton
            );
        }
    }

    private String getLocalCourse(List<String> localCourse){
        String allCourse = "<ul>";
        for(String getCourseName : localCourse){
            allCourse += "<li>" + getCourseName + "</li>";
        }
        allCourse += "</ul>";

        return allCourse;
    }

}
