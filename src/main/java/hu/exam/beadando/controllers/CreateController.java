package hu.exam.beadando.controllers;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.entity.Room;

public class CreateController extends AbstractCrudController {

    private Course createCourse = new Course();
    private Room createRoom = new Room();
    private Dialog createDialog = new Dialog();
    private Label dialogHeader = new Label("Hozzáadás");

    public void courseCrudCreate(){
        createDialog.open();
        initRoomComboBox(createCourse);

        confirmButton.addClickListener(buttonClickEvent -> {
            if(roomComboBox.isEmpty()){
                roomComboBox.setInvalid(true);
            }
            else if(courseBinder.writeBeanIfValid(createCourse)){
                courseRepository.courseSave(createCourse);
                createDialog.close();
                UI.getCurrent().getPage().reload();
            }
        });

        cancelButton.addClickListener(buttonClickEvent -> {
            createDialog.close();
            courseBinder.readBean(null);
        });

        if(courseBinder.isValid()) {
            courseBinder.readBean(createCourse);
            initCourseBinder();
        }

        initComponents(createDialog, dialogHeader);
        createDialog.add(
                dialogHeader,
                courseNameText,
                courseInstructorText,
                courseDatePicker,
                courseStartTimePicker,
                courseEndTimePicker,
                courseOnce,
                roomComboBox,
                confirmButton,
                cancelButton
        );
    }

    public void roomCrudCreate(){
        createDialog.open();

        confirmButton.addClickListener(buttonClickEvent -> {
            if(roomBinder.writeBeanIfValid(createRoom)){
                roomRepository.roomSave(createRoom);
                createDialog.close();
                UI.getCurrent().getPage().reload();
            }
        });

        cancelButton.addClickListener(buttonClickEvent -> {
            createDialog.close();
            roomBinder.readBean(null);
        });

        if(roomBinder.isValid()) {
            roomBinder.readBean(createRoom);
            initRoomBinder();
        }

        initComponents(createDialog, dialogHeader);
        createDialog.setHeight("300px");
        createDialog.add(
                dialogHeader,
                roomNameText,
                roomLevelText,
                confirmButton,
                cancelButton
        );
    }

}
