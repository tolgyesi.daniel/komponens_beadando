package hu.exam.beadando.controllers;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.value.ValueChangeMode;
import hu.exam.beadando.entity.Course;
import hu.exam.beadando.entity.Room;
import hu.exam.beadando.repository.CourseRepository;
import hu.exam.beadando.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class AbstractCrudController extends VerticalLayout{

    @Autowired
    public CourseRepository courseRepository = ApplicationContextHelper.getCourseRepository();

    @Autowired
    public RoomRepository roomRepository = ApplicationContextHelper.getRoomRepository();

    public Binder<Course> courseBinder = new Binder<>(Course.class);
    public Binder<Room> roomBinder = new Binder<>(Room.class);

    public TextField courseNameText = new TextField("Kurzus neve:");
    public TextField courseInstructorText = new TextField("Kurzus felelős:");
    public DatePicker courseDatePicker = new DatePicker("Kurzus napja:");
    public TimePicker courseStartTimePicker = new TimePicker("Kurzus kezdete:");
    public TimePicker courseEndTimePicker = new TimePicker("Kurzus vége:");
    public Button confirmButton = new Button("Mentés", new Icon(VaadinIcon.CHECK));
    public Button cancelButton = new Button("Mégse", new Icon(VaadinIcon.CLOSE));
    public Checkbox courseOnce = new Checkbox("Egyszer megtartandó kurzus");
    public ComboBox<Room> roomComboBox = new ComboBox<>("Tantermek");

    public TextField roomNameText = new TextField("Tanterem neve:");
    public TextField roomLevelText = new TextField("Szint:");

    public void initRoomComboBox(Course course){
        roomComboBox.setItemLabelGenerator(Room::getRoomName);
        List<Room> roomList = roomRepository.findAll();
        roomComboBox.setItems(roomList);

        roomComboBox.addValueChangeListener(event -> {
                course.setRoomId(roomComboBox.getValue());
        });
    }

    public void initRoomBinder(){
        roomBinder.forField(roomNameText)
                .asRequired("A tanterem nevét nem adta meg!")
                .bind(Room::getRoomName, Room::setRoomName);
        roomBinder.forField(roomLevelText)
                .asRequired("Nem adta meg a szintet!")
                .withConverter(new StringToIntegerConverter("Nem számot adott meg!"))
                .bind(Room::getRoomLevel, Room::setRoomLevel);
    }

    public void initCourseBinder(){
        courseBinder.forField(courseNameText)
                .asRequired("A kurzus nevét nem adta meg!")
                .bind(Course::getCourseName, Course::setCourseName);
        courseBinder.forField(courseInstructorText)
                .asRequired("A kurzus oktató nevét nem adta meg!")
                .bind(Course::getCourseInstructor, Course::setCourseInstructor);
        courseBinder.forField(courseDatePicker)
                .asRequired("Nem adott meg dátumot!")
                .bind(Course::getCourseTimeDate, Course::setCourseTimeDate);
        courseBinder.forField(courseStartTimePicker)
                .asRequired("Nem adta meg a kurzus kezdetét!")
                .bind(Course::getCourseTimeStart, Course::setCourseTimeStart);
        courseBinder.forField(courseEndTimePicker)
                .asRequired("Nem adta meg a kurzus végét!")
                .bind(Course::getCourseTimeEnd, Course::setCourseTimeEnd);
        courseBinder.forField(courseOnce)
                .bind(Course::getCourseOnce, Course::setCourseOnce);
        roomComboBox.setItemLabelGenerator(Room::getRoomName);
    }

    public void initComponents(Dialog dialog, Label dialogHeader){
        courseNameText.setValueChangeMode(ValueChangeMode.EAGER);
        courseInstructorText.setValueChangeMode(ValueChangeMode.EAGER);

        roomNameText.setValueChangeMode(ValueChangeMode.EAGER);
        roomLevelText.setValueChangeMode(ValueChangeMode.EAGER);

        HorizontalLayout actions = new HorizontalLayout();
        actions.add(confirmButton, cancelButton);

        confirmButton.getStyle().set("marginRight", "25%");
        confirmButton.getStyle().set("marginTop", "30px");
        confirmButton.getStyle().set("marginLeft", "15%");

        cancelButton.getStyle().set("marginTop", "30px");
        cancelButton.getStyle().set("color", "red");

        courseNameText.setRequiredIndicatorVisible(true);
        courseInstructorText.setRequiredIndicatorVisible(true);
        courseDatePicker.setRequiredIndicatorVisible(true);
        courseStartTimePicker.setRequiredIndicatorVisible(true);
        courseEndTimePicker.setRequiredIndicatorVisible(true);
        roomComboBox.setRequiredIndicatorVisible(true);
        roomComboBox.isRequired();
        roomComboBox.setErrorMessage("Nem választott ki tantermet!");

        roomNameText.setRequiredIndicatorVisible(true);
        roomLevelText.setRequiredIndicatorVisible(true);

        dialogHeader.getStyle().set("marginLeft", "-10px");
        dialogHeader.getStyle().set("fontWeight", "bold");
        dialogHeader.getStyle().set("fontSize", "14pt");
        courseNameText.getStyle().set("marginTop", "15px");
        roomNameText.getStyle().set("marginTop", "15px");

        courseNameText.setWidth("450px");
        courseInstructorText.setWidth("450px");
        courseDatePicker.setWidth("450px");
        courseStartTimePicker.getStyle().set("marginRight", "5px");
        courseStartTimePicker.setWidth("220px");
        courseEndTimePicker.setWidth("220px");
        courseOnce.setWidth("450px");
        roomComboBox.setWidth("450px");

        roomNameText.setWidth("450px");
        roomLevelText.setWidth("450px");

        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);
        dialog.setHeight("600px");
        dialog.setWidth("450px");
    }
}
