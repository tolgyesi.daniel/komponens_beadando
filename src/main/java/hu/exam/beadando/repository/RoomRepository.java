package hu.exam.beadando.repository;

import hu.exam.beadando.entity.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class RoomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void roomSave(Room room){
        entityManager.persist(room);
    }

    public void roomDelete(Room room){
        entityManager.remove(entityManager.find(Room.class, room.getRoomId()));
    }

    public void roomUpdate(Room room){
        entityManager.merge(room);
    }

    public List<Room> findAll(){
        return entityManager.createQuery("SELECT n FROM Room n", Room.class).getResultList();
    }

    public Room findOneById(Room id){
        TypedQuery<Room> query = entityManager.createQuery("SELECT n FROM Room n WHERE n.id = :id", Room.class);
        return query.setParameter("id", id.getRoomId()).getSingleResult();
    }

}
