package hu.exam.beadando.repository;

import hu.exam.beadando.entity.Course;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class CourseRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void courseSave(Course course){
        entityManager.persist(course);
    }

    public void courseDelete(Course course){
        entityManager.remove(entityManager.find(Course.class, course.getCourseId()));
    }

    public void courseUpdate(Course course){
        entityManager.merge(course);
    }

    public List<Course> findAll(){
        return entityManager.createQuery("SELECT n FROM Course n", Course.class).getResultList();
    }

    public List<Course> findCourseByRoomId(Long id){
        TypedQuery<Course> query = entityManager.createQuery("SELECT n FROM Course n WHERE room_id = :id", Course.class);
        return query.setParameter("id", id).getResultList();
    }

}
